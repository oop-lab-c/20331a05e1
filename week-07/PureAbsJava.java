import java.util.*;

interface sample {
    void show();

}

class PureAbsJava implements sample {
    public void show() {
        System.out.println("Java doesn't supports multiple inheritance... ");
    }

    public static void main(String[] args) {
        PureAbsJava h = new PureAbsJava();
        h.show();

    }

}
