class Functionoverloading
{
    void show(int a, int b)
    {
        System.out.println(a*b);
    }
    void show(int a, int b,int c)
    {
        System.out.println(a*b*c);
    }
    public static void main(String[] args)
    {
        Functionoverloading obj = new Functionoverloading();
        obj.show(7,9);
        obj.show(5,3,6);
    }
}
