import java.util.*;

abstract class base {
    base() {
        System.out.println("Working Of Abstract Class Constructor:\n");
    }

    abstract void print();

    void wish() {
        System.out.println("Happy Birthday...");
    }
}

class ParAbsJava extends base {
    void print() {
        System.out.println("Welcome to the world of java");
    }

    public static void main(String[] args) {

        ParAbsJava i = new ParAbsJava();
        i.wish();

        i.print();
    }

}
