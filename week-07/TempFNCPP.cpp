#include<iostream>
using namespace std;
template<class T>
T add(T &a,T &b)
{
    T result = a+b;
    return result;
}
int main()
{
    int a1=59;
    int a2=46;
    float m1=8.3;
    float m2=3.8;
    cout<<"the sum of a1 and a2 is "<<add(a1,a2)<<endl;
    cout<<"the sum of m1 and m2 is "<<add(m1,m2)<<endl;
    return 0;
}