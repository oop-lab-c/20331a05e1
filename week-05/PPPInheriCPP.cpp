#include<iostream>
using namespace std;
class parent
{
    public:
    int x;
    protected:
    int rollno;
    private:
    int radius;
    public:
    void area(int r)
    {
        int a;
        radius = r;
        a=3.14*radius*radius;
        cout<<"the radius is :"<<radius<<endl;
        cout<<"the area of circle is"<<a<<endl;
    }
    
};
class child:public parent
{
    public:
    void setrollno(int rn)
    {
        rollno=rn;
    }
    void display()
    {
        cout<<"the roll no is :"<<rollno<<endl;
    }
};
int main()
{
    parent obj;
    obj.x=18;
    cout<<"the value of x is :"<<obj.x<<endl;
    obj.area(18);
    child obj1;
    obj1.setrollno(12);
    obj1.display();
    
}