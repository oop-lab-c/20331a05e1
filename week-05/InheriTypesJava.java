class bird
{
    void fly()
    {
        System.out.println("I can fly in the sky");
    }
}
class parrot extends bird
{
    void eat()
    {
        System.out.println("I eat guava");
    }
}
class childparrot extends parrot // multiple inheitance 
{
    public static void main(String[] args)
    {
        childparrot p = new childparrot();
        p.fly();
        p.eat();
    }
}
class pigeon extends bird // simple inheritance
{
    void colour()
    {
        System.out.println("my colour is white");
    }
    public static void main(String[] args)
    {
        pigeon c = new pigeon();
        c.fly();
        c.colour();
    }
}