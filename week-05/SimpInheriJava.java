class animal
{
    void sound()
    {
        System.out.println("Sounds like  ");
    }
}
class cat extends animal
{
    void meow()
    {
        System.out.println("meow..meow..");
    }
    public static void main(String[] args)
    {
        cat c = new cat();
        c.sound();
        c.meow();
    }
}