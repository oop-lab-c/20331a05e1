#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<"The person A is called"<<endl;
    }
};
class father:virtual public person
{
    public:
    father()
    {
        cout<<"The father A is called"<<endl;
    }
};
class mother: virtual public person
{
    public:
    mother()
    {
        cout<<"The mother A is called"<<endl;
    }
};
class child:public father,public mother
{
    public:
    child()
    {
        cout<<"The child is called"<<endl;
    }
};
int main()
{
    child obj;
    return 0;
}