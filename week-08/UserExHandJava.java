import java.lang.*;//must import lang which contains exception class
import java.util.*;

class own extends Exception {// the exception class contains some methods like get message to use them
    own(String s)// we have to create default constructor or a string passed constructor
    {
        super(s);// this is required to get the methods
    }

}

class UserExHandJava {
    private static int a;

    static void validate() throws own {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter thr rank u have got:");
        a = sc.nextInt();
        if (a < 50000)
            throw new own("can admitted to the clg");
        else
            System.out.println("not admitted");

    }

    public static void main(String[] args) {
        try {
            validate();
        } catch (Exception e) {
            System.out.println("exception arised:\n" + e);
        }
    }
}
