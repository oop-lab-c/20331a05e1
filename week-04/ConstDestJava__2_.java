import java.util.Scanner;
public class Student
{
    String fullName;
    int rollNo;
    double semPercentage;
    String collegeName;
    int collegeCode;
    Student(String fn,int rn,double sp,String cn,int cc)
    {
        fullName= fn;
        rollNo=rn;
        semPercentage=sp;
        collegeName=cn;
        collegeCode=cc;
    }
    void display()
    {
        System.out.println(fullName+" "+rollNo+" "+semPercentage+" "+collegeName+" "+collegeCode);
    }
    public static void main(String[] args)
    {
        Student obj = new Student("teju",12,80.4,"mvgr",33);
        obj.display();
    }
}